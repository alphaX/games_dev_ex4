/*
 * CubeTexture.h
 *
 *  Created on: May 3, 2013
 *      Author: alex
 */

#ifndef CUBETEXTURE_H_
#define CUBETEXTURE_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <string>

using namespace std;
class CubeTexture {
public:
	CubeTexture(string baseFilePath, string format);
	GLuint getTextureBuffer();
	virtual ~CubeTexture();
private:
	GLuint texture;
	GLFWimage * images;
	GLuint loadCubeTexture(string nxFilePath,
			string pxFilePath,
			string nyFilePath,
			string pyFilePath,
			string nzFilePath,
			string pzFilePath);
	void loadCubeSide(string sideFilepath, GLuint side, int index);
};

#endif /* CUBETEXTURE_H_ */
