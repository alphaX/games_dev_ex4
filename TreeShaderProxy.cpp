/*
 * TreeShaderProxy.cpp
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#include "TreeShaderProxy.h"

TreeShaderProxy::TreeShaderProxy(CGcontext cgContext, const char * fxFilepath): ShaderProxy(cgContext, fxFilepath) {
	// TODO Auto-generated constructor stub
	feedbackVS = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "feedbackVS", 0);
	cgGLLoadProgram(feedbackVS);

	displayVS = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "displayVp", 0);
	cgGLLoadProgram(displayVS);

	feedbackGS = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgGprofile, "feedbackGS", 0);
	cgGLLoadProgram(feedbackGS);

	displayGS = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgGprofile, "displayGP", 0);
	cgGLLoadProgram(displayGS);

	displayFS = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgFprofile, "displayFp", 0);
	cgGLLoadProgram(displayFS);

	inflateGS = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgGprofile, "inflateFeedbackGP", 0);
	cgGLLoadProgram(inflateGS);

	feedbackVSpos = getParameter(feedbackVS, "pos");
	displayVSpos = getParameter(displayVS, "pos");
	displayVSWvp = getParameter(displayVS, "wvp");
	displayVSW = getParameter(displayVS, "w");
	displayVStreeTexture = getParameter(displayFS, "texture");
	feedbackGSrandomTex = getParameter(feedbackGS, "randomTex");
	feedbackGSTransformRand = getParameter(feedbackGS, "transformRand");
}

TreeShaderProxy::~TreeShaderProxy() {
	// TODO Auto-generated destructor stub
}



void TreeShaderProxy::setWvp(glm::mat4 wvp) {
	cgGLSetMatrixParameterfc(displayVSWvp, glm::value_ptr(wvp));
}

void TreeShaderProxy::setWv(glm::mat4 wv) {
	cgGLSetMatrixParameterfc(this->displayVSW, glm::value_ptr(wv));
}

void TreeShaderProxy::setPositionForGeneration(GLuint vertexBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	cgGLEnableClientState(feedbackVSpos);
	cgGLSetParameterPointer(feedbackVSpos, 4, GL_FLOAT, 0, 0);
}

void TreeShaderProxy::setPositionForDisplay(GLuint vertexBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	cgGLEnableClientState(displayVSpos);
	cgGLSetParameterPointer(displayVSpos, 4, GL_FLOAT, 0, 0);
}

void TreeShaderProxy::enableGeneration() {
	cgGLBindProgram(feedbackVS);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(feedbackGS);
	cgGLEnableProfile(cgGprofile);
}

void TreeShaderProxy::enableDisplay() {
	cgGLBindProgram(displayVS);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(displayGS);
	cgGLEnableProfile(cgGprofile);

	cgGLBindProgram(displayFS);
	cgGLEnableProfile(cgFprofile);
}

void TreeShaderProxy::enableInflate() {
	cgGLBindProgram(feedbackVS);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(inflateGS);
	cgGLEnableProfile(cgGprofile);
}

void TreeShaderProxy::postDisplayClean() {
	cgGLDisableClientState(displayVSpos);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgGprofile);
	cgGLDisableProfile(cgFprofile);
}

void TreeShaderProxy::postGenerationClean() {
	cgGLDisableClientState(feedbackVSpos);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgGprofile);
}

void TreeShaderProxy::setHeightRandScale(float scale) {
//	cgGLSetParameter1f(shaderHeightRandScale, scale);
}

void TreeShaderProxy::setTransformRandScale(float scale, float offset) {
	cgGLSetParameter2f(feedbackGSTransformRand, scale, offset);
}

void TreeShaderProxy::setRandomTexture(GLuint randTex) {
	cgGLEnableTextureParameter( feedbackGSrandomTex );
	cgGLSetupSampler( feedbackGSrandomTex, randTex);
	glBindTexture(GL_TEXTURE_2D, randTex); //Binding the texture
}

void TreeShaderProxy::setTreeTexutre(GLuint texture) {
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	cgGLEnableTextureParameter( displayVStreeTexture );
	cgGLSetupSampler( displayVStreeTexture, texture);
}


void TreeShaderProxy::postInflateClean() {
	cgGLDisableClientState(feedbackVSpos);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgGprofile);
}
