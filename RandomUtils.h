/*
 * RandomUtils.h
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#ifndef RANDOMUTILS_H_
#define RANDOMUTILS_H_
#include <stdlib.h>

class RandomUtils {
public:
	RandomUtils();
	virtual ~RandomUtils();

	static float frand();
};

#endif /* RANDOMUTILS_H_ */
