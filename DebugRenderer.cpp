/*
 * GLRenderer.cpp
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#include "DebugRenderer.h"


DebugRenderer::DebugRenderer(Entity & entity, Camera & camera, DebugShaderProxy * shaderProxy)
:entity(entity), camera(camera){
	this->shaderProxy = shaderProxy;
	bufferVertices();

}

void DebugRenderer::setProjection(glm::mat4 projection){
	this->projection = projection;
}

void DebugRenderer::render(){
	glBindBuffer(GL_ARRAY_BUFFER, verticeBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

	glm::mat4 world = entity.getWorldMatrix();
	glm::mat4 view = camera.getViewMatrix();
	glm::mat4 wvp = projection * view * world;

	shaderProxy->preRender();
	shaderProxy->setWvp(wvp);
	shaderProxy->setPositionWithCurrentBuffers();

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, this->entity.getMesh().getIndexesNumber()*3, GL_UNSIGNED_INT, (GLvoid*)0);
	shaderProxy->postRenderClean();
}

void DebugRenderer::bufferVertices(){

	std::vector<glm::vec3> & vertices = entity.getMesh().getVertices();
	glGenBuffers(1, &verticeBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, verticeBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	printf("bufferd %d vertices, first one is (%f,%f, %f)\n", vertices.size(), vertices.at(1).x, vertices.at(1).y, vertices.at(1).z);
	std::vector<glm::uvec3> & indexes = entity.getMesh().getIndexes();
	glGenBuffers(1, &this->indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexes.size() * sizeof(glm::uvec3), &indexes[0], GL_STATIC_DRAW);
	printf("bufferd %d indexes, first one is (%d,%d, %d)\n", indexes.size(), indexes.at(0).x, indexes.at(0).y, indexes.at(0).z);
}

DebugRenderer::~DebugRenderer() {
	glDeleteBuffers(1, &(verticeBuffer));
	glDeleteBuffers(1, &(indexBuffer));
}
