/*
 * TreeShaderProxy.h
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#ifndef TREESHADERPROXY_H_
#define TREESHADERPROXY_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <Cg/cgGL.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

#include "ShaderProxy.h"

class TreeShaderProxy: public ShaderProxy {
public:
	TreeShaderProxy(CGcontext cgContext, const char * fxFilepath);
	virtual ~TreeShaderProxy();

	void setWvp(glm::mat4 wvp);
	void setWv(glm::mat4 wv);

	void setPositionForGeneration(GLuint vertexBuffer);
	void setPositionForDisplay(GLuint vertexBuffer);
	void setRandomTexture(GLuint randTex);
	void setHeightRandScale(float scale);
	void setTransformRandScale(float xScale, float ySclae);
	void setTreeTexutre(GLuint texture);
	void enableGeneration();
	void enableInflate();
	void enableDisplay();
	void postGenerationClean();
	void postDisplayClean();
	void postInflateClean();

private:
	CGprogram feedbackVS, displayVS, feedbackGS, displayGS, displayFS, inflateGS;
	CGparameter feedbackVSpos, displayVSpos, displayVSWvp, displayVSW, displayVStreeTexture, feedbackGSrandomTex, feedbackGSTransformRand;
};

#endif /* TREESHADERPROXY_H_ */
