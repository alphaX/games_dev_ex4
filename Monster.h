/*
 * Monster.h
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#ifndef MONSTER_H_
#define MONSTER_H_
#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <vector>
#include "RandomTextureGenerator.h"
#include "MonsterShaderProxy.h"

using namespace glm;

class MonsterJoint {
public:
	MonsterJoint(MonsterShaderProxy & shaderProxy);
	virtual ~MonsterJoint();

	void setWorldMatrix(mat4 world);
	mat4 getWorldMatrix();

	void update();
	GLuint getBuffer();
	vec3 getPosition();
	void setPosition(vec3 position);
	vec3 getDirection();
	void setDirection(vec3 direction);

	int getNumberOfVertices();
private:
	vec3 position;
	vec3 direction;
	mat4 worldMatrix;

	MonsterShaderProxy shaderProxy;
	GLuint vertexBuffer[2];
	int currentBuffer;
	GLuint primitivesWritten;
	GLuint randTex;
	GLuint query;
};

class Monster{
public:
	Monster(MonsterShaderProxy & shaderProxy);
	void update(float dt);
	void setWorldMatrix(mat4 world);
	mat4 getWorldMatrixOfJoint(int i);
	int getNumberOfJoints();
	GLuint getJointsBuffers(int i);
	int getNumberOfJointVertices(int i);
	void setPosition(vec3 position);
private:
	std::vector<MonsterJoint> joints;
	mat4 worldMatrix;
	float speed;

};

#endif /* MONSTER_H_ */
