/*
 * SinWaveShaderProxy.cpp
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#include "SinWaveShaderProxy.h"


SinWaveShaderProxy::SinWaveShaderProxy(CGcontext cgContext, const char * fxFilepath):ShaderProxy(cgContext, fxFilepath) {
	vertexShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "sinWaveVS", 0);
	cgGLLoadProgram(vertexShader);
	pixelShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgFprofile, "sinWavePS", 0);
	cgGLLoadProgram(pixelShader);


	cgPosition = cgGetNamedParameter(vertexShader, "IN.position");
	cgWvp = cgGetNamedParameter(vertexShader, "wvp");
	cgWorld = cgGetNamedParameter(vertexShader, "world");
	cgEyePos = cgGetNamedParameter(vertexShader, "eyePosition");
	cgTime = cgGetNamedParameter(vertexShader, "time");

	cgEnvSamper = cgGetNamedParameter(pixelShader, "envSam");
}

void SinWaveShaderProxy::setWvp(glm::mat4 wvp){
	cgGLSetMatrixParameterfc(this->cgWvp, glm::value_ptr(wvp));
}

void SinWaveShaderProxy::setPositionWithCurrentBuffers(){
	cgGLEnableClientState(cgPosition);
	cgGLSetParameterPointer(cgPosition, 3, GL_FLOAT, 0, 0);
}

void SinWaveShaderProxy::setTime(double time){
	cgGLSetParameter1d(cgTime, time);
}

void SinWaveShaderProxy::preRender(){
	cgGLBindProgram(this->vertexShader);    // note - need vertex program for generic attribs to work!
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(this->pixelShader);
	cgGLEnableProfile(cgFprofile);
}

void SinWaveShaderProxy::setWorldMatrix(glm::mat4 world) {
	cgGLSetMatrixParameterfc(cgWorld, glm::value_ptr(world));
}

void SinWaveShaderProxy::setEyePos(glm::vec3 eyePos) {
	cgGLSetParameter3fv(cgEyePos, glm::value_ptr(eyePos));
}

void SinWaveShaderProxy::postRenderClean(){
	cgGLDisableClientState(cgPosition);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgFprofile);
}

void SinWaveShaderProxy::setTexture(GLuint tex){
	cgGLEnableTextureParameter( cgEnvSamper );
	cgGLSetupSampler( cgEnvSamper, tex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex); //Binding the texture
}

SinWaveShaderProxy::~SinWaveShaderProxy() {
	// TODO Auto-generated destructor stub
}

