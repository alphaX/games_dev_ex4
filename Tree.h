/*
 * Tree.h
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#ifndef TREE_H_
#define TREE_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include "BufferedEntity.h"
#include "Texture2D.h"
class Tree: public BufferedEntity {
public:
	Tree(GLuint buffer, int numberOfVertices);
	void setTexture(Texture2D & texture);
	GLuint getTexture();
	virtual ~Tree();
private:
	GLuint texture;
};

#endif /* TREE_H_ */
