/*
 * BufferedEntity.cpp
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#include "BufferedEntity.h"

BufferedEntity::BufferedEntity(GLuint buffer, int numberOfVertexes) {
	// TODO Auto-generated constructor stub
	this->buffer = buffer;
	this->numberOfVertexes = numberOfVertexes;
}

BufferedEntity::~BufferedEntity() {
	// TODO Auto-generated destructor stub
}

void BufferedEntity::setWorldMatrix(glm::mat4 world) {
	this->world = world;
}

glm::mat4 BufferedEntity::getWorldMatrix() {
	return world;
}

int BufferedEntity::getNumberOfVertexes() {
	return numberOfVertexes;
}

GLuint BufferedEntity::getBuffer() {
	return buffer;
}
