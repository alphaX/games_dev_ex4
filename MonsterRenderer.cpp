/*
 * MonsterRenderer.cpp
 *
 *  Created on: May 14, 2013
 *      Author: alex
 */

#include "MonsterRenderer.h"


MonsterRenderer::MonsterRenderer(Monster & monster, Camera & camera, MonsterShaderProxy shaderProxy):
monster(monster), camera(camera), shaderProxy(shaderProxy){
}

MonsterRenderer::~MonsterRenderer() {
	// TODO Auto-generated destructor stub
}

void MonsterRenderer::render() {
	glm::mat4 view = camera.getViewMatrix();
	glm::mat4 vp = projection * view;

	//printf("render %d joints\n", monster.getNumberOfJoints());
	for(int i = 0; i< monster.getNumberOfJoints(); i++){
		//printf("render joint %d with %d vertices\n", i, monster.getNumberOfJointVertices(i));
		glBindBuffer(GL_ARRAY_BUFFER, monster.getJointsBuffers(i));

		glm::mat4 world = monster.getWorldMatrixOfJoint(i);
		glm::mat4 wvp = vp * world;

		shaderProxy.enableDisplay();
		shaderProxy.setWvp(wvp);
		shaderProxy.setPositionForDisplay(monster.getJointsBuffers(i));
		glPointSize(15);
		glShadeModel(GL_FLAT);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDrawArrays(GL_POINTS, 0, monster.getNumberOfJointVertices(i));

		shaderProxy.postDisplayClean();
	}
}
