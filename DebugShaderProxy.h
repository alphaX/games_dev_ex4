/*
 * DebugShaderProxy.h
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#ifndef DEBUGSHADERPROXY_H_
#define DEBUGSHADERPROXY_H_


#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

#include "ShaderProxy.h"

class DebugShaderProxy: public ShaderProxy {
public:
	DebugShaderProxy(CGcontext cgContext, const char * fxFilepath);
	virtual ~DebugShaderProxy();

	void setWvp(glm::mat4 wvp);
	void setPositionWithCurrentBuffers();
	void preRender();
	void postRenderClean();

private:
	CGprogram vertexShader;
	CGprogram pixelShader;
	CGparameter wvp;
	CGparameter position;

	CGprofile cgVprofile;
	CGprofile cgFprofile;
};

#endif /* DEBUGSHADERPROXY_H_ */
