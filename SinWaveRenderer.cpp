#include "SinWaveRenderer.h"

SinWaveRenderer::SinWaveRenderer(Entity & entity, Camera & camera,
		Clock & clock, CubeTexture & envTex, SinWaveShaderProxy * shaderProxy) :
		entity(entity), camera(camera), clock(clock), envTexture(envTex) {

	this->shaderProxy = shaderProxy;
	bufferVertices();

}

void SinWaveRenderer::render() {
	glBindBuffer(GL_ARRAY_BUFFER, verticeBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

	glm::mat4 world = entity.getWorldMatrix();
	glm::mat4 view = camera.getViewMatrix();
	glm::mat4 wvp = projection * view * world;

	shaderProxy->preRender();
	shaderProxy->setWvp(wvp);
	shaderProxy->setPositionWithCurrentBuffers();
	shaderProxy->setTime(clock.getTime());
	shaderProxy->setWorldMatrix(world);
	shaderProxy->setEyePos(camera.getPosition());
	shaderProxy->setTexture(this->envTexture.getTextureBuffer());
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawElements(GL_TRIANGLES, this->entity.getMesh().getIndexesNumber() * 3,
			GL_UNSIGNED_INT, (GLvoid*) 0);
	shaderProxy->postRenderClean();
}

void SinWaveRenderer::bufferVertices() {
	std::vector<glm::vec3> & vertices = entity.getMesh().getVertices();
	glGenBuffers(1, &verticeBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, verticeBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3),
			&vertices[0], GL_STATIC_DRAW);

	std::vector<glm::uvec3> & indexes = entity.getMesh().getIndexes();
	glGenBuffers(1, &this->indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexes.size() * sizeof(glm::uvec3),
			&indexes[0], GL_STATIC_DRAW);
}

SinWaveRenderer::~SinWaveRenderer() {
	glDeleteBuffers(1, &(verticeBuffer));
	glDeleteBuffers(1, &(indexBuffer));
}
