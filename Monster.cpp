/*
 * Monster.cpp
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#include "Monster.h"
#include <glm/gtx/vector_angle.hpp>

MonsterJoint::MonsterJoint(MonsterShaderProxy & shaderProxy): shaderProxy(shaderProxy) {
	// TODO Auto-generated constructor stub
	currentBuffer = 0;
	primitivesWritten = 1;

	int max_buffer_verts = 1<<20;
	glGenBuffers(2, vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, max_buffer_verts*4*sizeof(GLfloat), 0, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[1]);
	glBufferData(GL_ARRAY_BUFFER, max_buffer_verts*4*sizeof(GLfloat), 0, GL_STATIC_DRAW);

	float emitters[] = {0, 0, 0 ,1};
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[currentBuffer]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, 4*sizeof(GLfloat), emitters);

	randTex = RandomTextureGenerator::generateWhiteNoise(256,256);

	GLint attribs[] = { GL_POSITION, 4, 0};
	glTransformFeedbackAttribsNV(1, attribs, GL_INTERLEAVED_ATTRIBS_NV);

	glGenQueries(1, &query);
}

MonsterJoint::~MonsterJoint() {
	// TODO Auto-generated destructor stub
}

void MonsterJoint::update() {
	glBindBufferOffsetNV(GL_TRANSFORM_FEEDBACK_BUFFER_NV, 0, vertexBuffer[1-currentBuffer], 0);

	glBeginTransformFeedbackNV(GL_POINTS);
	glEnable(GL_RASTERIZER_DISCARD_NV);    // disable rasterization

	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV, query);

	//printf("enabled monster generation\n");
	shaderProxy.enableGeneration();

	shaderProxy.setRandomTexture(randTex);
	shaderProxy.setRandTransform(rand() / (float) RAND_MAX*2.0-1.0, rand() / (float) RAND_MAX*2.0-1.0);

	//printf("set monster random texture\n");
	shaderProxy.setPositionForFeedback(vertexBuffer[currentBuffer]);
	//printf("set monster generation position\n");
	glDrawArrays(GL_POINTS, 0, primitivesWritten);
	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV);

	// read back query results
	glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitivesWritten);

	//printf("generated %d particales\n", primitivesWritten);
	shaderProxy.postGenerationClean();
	//printf("clean after monster generation\n");
	glDisable(GL_RASTERIZER_DISCARD_NV);
	glEndTransformFeedbackNV();

	currentBuffer = 1 - currentBuffer;

}

GLuint MonsterJoint::getBuffer() {
	return vertexBuffer[currentBuffer];
}

void MonsterJoint::setWorldMatrix(mat4 world) {
	worldMatrix = world;
}


/**
 * matrix from joint space to monster space
 */
mat4 MonsterJoint::getWorldMatrix() {
	//mat4 rotation = lookAt(vec3(0.0f,0.0f,0.0f), -direction, vec3(0.0f,1.0f,0.0f));
	vec3 z = vec3(0,0,1);
	float angle = glm::angle(z, direction);
	vec3 axis = cross(z, direction);
	mat4 rotation = rotate(mat4(4), angle, axis);
	mat4 translation = translate(mat4(1.0), position);

	return   translation * rotation;
	//return translation;
}

vec3 MonsterJoint::getPosition() {
	return position;
}

vec3 MonsterJoint::getDirection() {
	return direction;
}

void MonsterJoint::setDirection(vec3 direction) {
	this->direction = direction;
}

void MonsterJoint::setPosition(vec3 position) {
	this->position = position;
}

int MonsterJoint::getNumberOfVertices() {
	return primitivesWritten;
}

Monster::Monster(MonsterShaderProxy& shaderProxy) {
	int numberOfJoints = 8;
	speed = 15;
	for(int i = 0; i< numberOfJoints; i++){
		MonsterJoint joint(shaderProxy);
		joint.setDirection(vec3(0,0,-1));
		//joint.setPosition(vec3(0,0,-i));
		joints.push_back(joint);
	}
}

void Monster::setPosition(vec3 position){
	for(unsigned int i=0; i< joints.size(); i++){
		joints[i].setPosition(position - vec3(0,0,i));
		joints[i].setDirection(vec3(0,0,1));
	}
}

void Monster::update(float dt) {
	float a = 40;
	vec3 previouseJointPosition;

	for(unsigned int i=0; i<joints.size(); i++){
		vec3 currentPosition = joints[i].getPosition();
		vec3 currentDirection = joints[i].getDirection();

		vec3 nextDirection, nextPosition;
		if(i==0){
			nextDirection =  normalize(rotate(currentDirection, a * dt, vec3(0.0f,1.0f,0.0f)));
			previouseJointPosition = currentPosition;
			nextPosition = currentPosition + nextDirection * speed * dt;
		}
		else{
			//nextDirection = previouseJointDirection;
			nextPosition = previouseJointPosition;
			nextDirection = normalize(nextPosition - currentPosition);
			previouseJointPosition = currentPosition;
		}

		joints[i].setPosition(nextPosition);
		joints[i].setDirection(nextDirection);

		joints[i].update();
	}
}

void Monster::setWorldMatrix(mat4 world) {
	worldMatrix = world;
}

mat4 Monster::getWorldMatrixOfJoint(int i) {
	mat4 worldMatOfJoint = joints[i].getWorldMatrix();
	//worldMatOfJoint = translate(worldMatOfJoint, vec3(0, 0, -i));
	//return  worldMatOfJoint * worldMatrix;
	return  worldMatOfJoint;
}

int Monster::getNumberOfJoints() {
	return joints.size();
}

GLuint Monster::getJointsBuffers(int i) {
	return joints[i].getBuffer();
}

int Monster::getNumberOfJointVertices(int i) {
	return joints[i].getNumberOfVertices();
}
