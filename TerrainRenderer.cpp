/*
 * TerrainRenderer.cpp
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#include "TerrainRenderer.h"
#include "RandomUtils.h"
TerrainRenderer::TerrainRenderer(Terrain & t, Camera & c, TerrainShaderProxy & shaderProxy):
terrain(t), camera(c), shaderProxy(shaderProxy){
	// TODO Auto-generated constructor stub

}

void TerrainRenderer::render() {
	glBindBuffer(GL_ARRAY_BUFFER, terrain.getBuffer());

	glm::mat4 world = terrain.getWorldMatrix();
	glm::mat4 view = camera.getViewMatrix();
	glm::mat4 wvp = projection * view * world;
	glm::mat4 wv =  world;
	shaderProxy.enableDisplay();
	shaderProxy.setWvp(wvp);
	shaderProxy.setWv(wv);
	shaderProxy.setPositionForDisplay(terrain.getBuffer());
	shaderProxy.setTexutres(terrain.getLevel1Texture(), terrain.getLevel2Texture());
	shaderProxy.setTransformRandScale(RandomUtils::frand(), RandomUtils::frand());
	shaderProxy.setCameraLocation(camera.getPosition());
	glShadeModel(GL_FLAT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawArrays(GL_LINES_ADJACENCY_EXT, 0, this->terrain.getNumberOfVertexes());

	shaderProxy.postDisplayClean();
}

TerrainRenderer::~TerrainRenderer() {
	// TODO Auto-generated destructor stub
}

