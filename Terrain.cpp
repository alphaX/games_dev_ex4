/*
 * Terrain.cpp
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#include "Terrain.h"

Terrain::Terrain(GLuint buffer, int numberOfVertexes) : BufferedEntity(buffer, numberOfVertexes) {
}

Terrain::~Terrain() {
	// TODO Auto-generated destructor stub
}

void Terrain::setTextures(Texture2D & level1, Texture2D & level2) {
	level1Texture = level1.getTextureBuffer();
	level2Texture = level2.getTextureBuffer();
}

GLuint Terrain::getLevel1Texture() {
	return level1Texture;
}

GLuint Terrain::getLevel2Texture() {
	return level2Texture;
}
