/*
 * TerrainShaderProxy.h
 *
 *  Created on: May 10, 2013
 *      Author: alex
 */

#ifndef TERRAINSHADERPROXY_H_
#define TERRAINSHADERPROXY_H_
#include <GL/glew.h>
#include <GL/glfw.h>

#include <Cg/cgGL.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

#include "ShaderProxy.h"
class TerrainShaderProxy: public ShaderProxy {
public:
	TerrainShaderProxy(CGcontext cgContext, const char * fxFilepath);
	virtual ~TerrainShaderProxy();

	void setWvp(glm::mat4 wvp);
	void setWv(glm::mat4 wv);
	void setPositionForGeneration(GLuint vertexBuffer);
	void setPositionForDisplay(GLuint vertexBuffer);
	void setRandomTexture(GLuint randTex);
	void setHeightRandScale(float scale);
	void setTransformRandScale(float xScale, float ySclae);
	void setTexutres(GLuint l1, GLuint l2);
	void enableGeneration();
	void enableDisplay();
	void postGenerationClean();
	void postDisplayClean();
	void setCameraLocation(glm::vec3 cameraLocation);

private:
	CGprogram passthruVertexShader, transformVertexShader,  pixelShader, generationShader, terrainDisplayShader, debugDisplayShader, displayFragmentShader;
	CGparameter passthruShaderPosition, shaderRandomTexture,
		transformShaderPosition, shaderWVP, shaderHeightRandScale, shaderTransformRandScale,
		shaderWV, level1Texture, level2Texture, cameraLocation;

};

#endif /* TERRAINSHADERPROXY_H_ */
