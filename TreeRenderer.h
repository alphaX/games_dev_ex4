/*
 * TreeRenderer.h
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#ifndef TREERENDERER_H_
#define TREERENDERER_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include "GLRenderer.h"
#include "Tree.h"
#include "Camera.h"
#include "TreeShaderProxy.h"

class TreeRenderer: public GLRenderer {
public:
	TreeRenderer(Tree & tree, Camera & camera, TreeShaderProxy & shaderProxy);
	virtual ~TreeRenderer();

	void render();

private:
	Tree & tree;
	Camera & camera;
	TreeShaderProxy & shaderProxy;
};

#endif /* TREERENDERER_H_ */
