/*
 * EntityFactory.cpp
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#include "EntityFactory.h"
#include <glm/gtc/matrix_transform.hpp>
EntityFactory::EntityFactory() {
	// TODO Auto-generated constructor stub

}

Entity EntityFactory::createSkybox(){
	vector<vec3> * vetrices = new vector<vec3>();

	vetrices->push_back(vec3(1.0f, -1.0f, -1.0f));
	vetrices->push_back(vec3(1.0f, -1.0f, 1.0f));
	vetrices->push_back(vec3(-1.0f, -1.0f, 1.0f));
	vetrices->push_back(vec3(-1.0f, -1.0f, -1.0f));
	vetrices->push_back(vec3(1.0f, 1.0f, -1.0f));
	vetrices->push_back(vec3(1.0f, 1.0f, 1.0f));
	vetrices->push_back(vec3(-1.0f, 1.0f, 1.0f));
	vetrices->push_back(vec3(-1.0f, 1.0f, -1.0f));

	vector<uvec3> * indexes = new vector<uvec3>();
	// bottom
	indexes->push_back(uvec3(0,1,2));
	indexes->push_back(uvec3(0,2,3));

	// top
	indexes->push_back(uvec3(4,5,6));
	indexes->push_back(uvec3(4,6,7));

	// back
	indexes->push_back(uvec3(1,5,2));
	indexes->push_back(uvec3(2,5,6));

	// front
	indexes->push_back(uvec3(0,3,4));
	indexes->push_back(uvec3(3,4,7));

	// right
	indexes->push_back(uvec3(0,1,4));
	indexes->push_back(uvec3(1,4,5));

	// left
	indexes->push_back(uvec3(2,3,6));
	indexes->push_back(uvec3(3,6,7));

	vector<vec3> * texCoords = new vector<vec3>();

	for(unsigned int i = 0; i < vetrices->size(); i++){
		vec3 coord = vetrices->at(i);
		coord.y *= -1.0f;
		texCoords->push_back(coord);
	}

	std::vector<glm::vec3> * normals = new std::vector<glm::vec3>();

	Mesh * cube = new Mesh(*vetrices, *indexes, *normals);

	//CubeTexture * skyboxTexture = new CubeTexture("textures/debugSkybox/debug", "tga");
	CubeTexture * skyboxTexture = new CubeTexture("textures/skybox/skybox", "tga");
	Entity cubeEntity(*cube);
	cubeEntity.applyTexture(skyboxTexture, *texCoords);
	cubeEntity.setWorldMatrix(scale(mat4(1), vec3(50.0f, 50.0f, 50.0f)));

	return cubeEntity;
}

Entity EntityFactory::createPlane(int gridSizeX, int gridSizeZ, float gridDensityX, float gridDensityZ){
	// create vertices
	float numberOfCellsX = gridSizeX/gridDensityX;

	float numberOfCellsZ = gridSizeZ/gridDensityZ;

	vector<vec3> * vertices = new vector<vec3>();
	for(float x = -gridSizeX/2.0; x<= gridSizeX/2.0; x+=gridDensityX){
		for(float z = -gridSizeZ/2.0; z <= gridSizeZ/2.0; z+=gridDensityZ){
			vertices->push_back(vec3(x, 0, z));

		}
	}

	// create indexes
	vector<uvec3> * indexes = new vector<uvec3>();
	int numberOfVerticesZ = numberOfCellsZ + 1;

	for(int iX = 0; iX < numberOfCellsX; iX++){
		for(int iZ = 0; iZ < numberOfCellsZ; iZ++){
			indexes->push_back(uvec3(
				iX + numberOfVerticesZ * iZ,
				iX + numberOfVerticesZ * iZ + 1,
				iX + numberOfVerticesZ * (iZ + 1)
			));

			indexes->push_back(uvec3(
				iX + numberOfVerticesZ * iZ + 1,
				iX + numberOfVerticesZ * (iZ + 1),
				iX + numberOfVerticesZ * (iZ + 1) + 1
			));
		}
	}

	vector<vec3> * normals = new vector<vec3>();
	Mesh * planeMesh = new Mesh(*vertices, *indexes,* normals);
	Entity planeEntity(*planeMesh);

	return planeEntity;
}

EntityFactory::~EntityFactory() {
	// TODO Auto-generated destructor stub
}

