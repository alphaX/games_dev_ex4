/*
 * RandomTextureGenerator.cpp
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#include "RandomTextureGenerator.h"
#include "RandomUtils.h"

RandomTextureGenerator::RandomTextureGenerator() {
	// TODO Auto-generated constructor stub

}

GLuint RandomTextureGenerator::generateWhiteNoise(int w, int h) {
	float *data = new float [w*h*4];
	float *ptr = data;
	for(int i=0; i<w*h; i++) {
		*ptr++ = RandomUtils::frand()*2.0-1.0;
		*ptr++ = RandomUtils::frand()*2.0-1.0;
		*ptr++ = RandomUtils::frand()*2.0-1.0;
		*ptr++ = RandomUtils::frand()*2.0-1.0;

	}
	GLuint texid;
	glGenTextures(1, &texid);
	glBindTexture(GL_TEXTURE_2D, texid);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F_ARB, w, h, 0, GL_RGBA, GL_FLOAT, data);

	return texid;
}

RandomTextureGenerator::~RandomTextureGenerator() {
	// TODO Auto-generated destructor stub
}

