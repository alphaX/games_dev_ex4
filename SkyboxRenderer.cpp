/*
 * SkyBoxRenderer.cpp
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#include "SkyboxRenderer.h"

SkyboxRenderer::SkyboxRenderer(Entity & entity, Camera & camera, SkyboxShaderProxy * shaderProxy):
	entity(entity), camera(camera) {
	this->shaderProxy = shaderProxy;
	bufferVertices();
	bufferTexture();

}

void SkyboxRenderer::bufferVertices(){
	std::vector<glm::vec3> & vertices = entity.getMesh().getVertices();
	glGenBuffers(1, &verticeBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, verticeBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	std::vector<glm::uvec3> & indexes = entity.getMesh().getIndexes();
	glGenBuffers(1, &this->indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexes.size() * sizeof(glm::uvec3), &indexes[0], GL_STATIC_DRAW);
}

void SkyboxRenderer::bufferTexture(){
	std::vector<glm::vec3> & texCoords = entity.getTextureCoords();
	glGenBuffers(1, &textureCoordsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, textureCoordsBuffer);
	glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(glm::vec3), &texCoords[0], GL_STATIC_DRAW);
}

void SkyboxRenderer::render(){
	glm::mat4 world = entity.getWorldMatrix();
	glm::mat4 view = camera.getViewMatrix();
	view[3] = vec4(0,0,0,1);
	glm::mat4 wvp = projection * view * world;
	glDisable(GL_DEPTH_TEST);
	glDepthMask(false);
	shaderProxy->enable();
	shaderProxy->setPosition(verticeBuffer,indexBuffer);
	shaderProxy->setTexture(this->entity.getTextureBuffer());
	shaderProxy->setTextureCoordinates(this->textureCoordsBuffer);
	shaderProxy->setWvp(wvp);

	glBindBuffer(GL_ARRAY_BUFFER, verticeBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawElements(GL_TRIANGLES, this->entity.getMesh().getIndexesNumber()*3, GL_UNSIGNED_INT, (GLvoid*)0);
	shaderProxy->postRenderClean();
	glEnable(GL_DEPTH_TEST);
	glDepthMask(true);
}


SkyboxRenderer::~SkyboxRenderer() {
	// TODO Auto-generated destructor stub
}

