/*
 * Texture2D.h
 *
 *  Created on: May 16, 2013
 *      Author: alex
 */

#ifndef TEXTURE2D_H_
#define TEXTURE2D_H_

#include <GL/glew.h>
#include <GL/glfw.h>

class Texture2D {
public:
	Texture2D(const char * filepath);
	GLuint getTextureBuffer();
	virtual ~Texture2D();
private:
	GLuint texture;
};

#endif /* TEXTURE2D_H_ */
