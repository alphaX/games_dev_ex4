/*
 * CubeTexture.cpp
 *
 *  Created on: May 3, 2013
 *      Author: alex
 */

#include "CubeTexture.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
CubeTexture::CubeTexture(std::string baseFilePath, std::string format) {
	// TODO Auto-generated constructor stub
	loadCubeTexture(
		baseFilePath + "_xn." + format,
		baseFilePath + "_xp." + format,
		baseFilePath + "_yn." + format,
		baseFilePath + "_yp." + format,
		baseFilePath + "_zn." + format,
		baseFilePath + "_zp." + format
	);
}

GLuint CubeTexture::getTextureBuffer(){
	return texture;
}

CubeTexture::~CubeTexture() {
	// TODO Auto-generated destructor stub
}

GLuint CubeTexture::loadCubeTexture(
		string nxFilePath,
		string pxFilePath,
		string nyFilePath,
		string pyFilePath,
		string nzFilePath,
		string pzFilePath) {

	glEnable(GL_TEXTURE_CUBE_MAP);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	images = new GLFWimage[6];
	loadCubeSide(nxFilePath,GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0);
	loadCubeSide(pxFilePath,GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0);
	loadCubeSide(pyFilePath,GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0);
	loadCubeSide(nyFilePath,GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0);
	loadCubeSide(nzFilePath,GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0);
	loadCubeSide(pzFilePath,GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0);

	return texture;
}

void CubeTexture::loadCubeSide(string sideFilepath, GLuint side, int index){
	GLFWimage image = images[index];
	if(!glfwReadImage(sideFilepath.c_str(), & image, 0)){
		fprintf(stderr, "Error opening %s\n", sideFilepath.c_str());
		exit(EXIT_FAILURE);
	}

	glTexImage2D(side, 0, GL_RGBA, image.Width, image.Height, 0, image.BytesPerPixel == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, image.Data);
}
