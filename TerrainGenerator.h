/*
 * TerrainGenerator.h
 *
 *  Created on: May 10, 2013
 *      Author: alex
 */

#ifndef TERRAINGENERATOR_H_
#define TERRAINGENERATOR_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "TerrainShaderProxy.h"
#include "Terrain.h"

class TerrainGenerator {
public:
	TerrainGenerator(TerrainShaderProxy & shaderProxy);
	virtual ~TerrainGenerator();

	Terrain generateTerrain(int);
	Terrain generateSimplexTerrain(int w, int h, float gridDensityX, float gridDensityZ );
	Terrain generateLOD(glm::vec3 location);
private:
	GLuint vertexBuffer[2];
	int currentBuffer;
	GLuint primitivesWritten;
	TerrainShaderProxy & shaderProxy;
	const int vertexesPerPrimitive;
	const int primOutputDiv;
	GLuint randomTexture;
	GLfloat heightRandomScale;
	void feedbackIteration();
	void generateBase();
};

#endif /* TERRAINGENERATOR_H_ */
