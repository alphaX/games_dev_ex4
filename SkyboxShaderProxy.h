/*
 * SkyboxShaderProxy.h
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#ifndef SKYBOXSHADERPROXY_H_
#define SKYBOXSHADERPROXY_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

#include "ShaderProxy.h"

class SkyboxShaderProxy : public ShaderProxy {
public:
	SkyboxShaderProxy(CGcontext cgContext,const char * fxFilepath);
	virtual ~SkyboxShaderProxy();

	void setWvp(glm::mat4 wvp);
	void setTexture(GLuint tex);
	void setTextureCoordinates(GLuint texCoordsBuffer);
	void setPosition(GLuint vertexBuffer, GLuint indexBuffer);
	void enable();
	void postRenderClean();
private:
	CGprogram vertexShader;
	CGprogram pixelShader;
	CGparameter wvp;
	CGparameter position;
	CGparameter cubeSampler;
	CGparameter textureCoords;
};

#endif /* SKYBOXSHADERPROXY_H_ */
