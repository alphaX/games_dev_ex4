/*
 * TreeGenerator.h
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#ifndef TREEGENERATOR_H_
#define TREEGENERATOR_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "TreeShaderProxy.h"
#include "Tree.h"
#include "Texture2D.h"

class TreeGenerator {
public:
	TreeGenerator(TreeShaderProxy & shaderProxy);
	virtual ~TreeGenerator();

	Tree generateTree(int levels);
private:
	GLuint vertexBuffer[2];
	int currentBuffer;
	GLuint primitivesWritten;
	TreeShaderProxy & shaderProxy;
	const int vertexesPerPrimitive;
	const int primOutputDiv;
	GLuint randomTexture;
	GLfloat heightRandomScale;

	void feedbackIteration();
	void generateBase();
	void inflateTree();
};

#endif /* TREEGENERATOR_H_ */
