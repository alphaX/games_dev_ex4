/*
 * player.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */

#include "Player.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

Player::Player(PlayerPropeties properties){
	this->properties = properties;
	this->turnDirection = glm::vec3(0,0,1);
	this->position = glm::vec3(0,50,-5);
	this->currentSpeed = glm::vec3(0,0,0);
}

void Player::advance(Direction direction){
	this->direction = direction;
	switch(direction){
	case FORWARD:
		this->currentSpeed = this->turnDirection*this->properties.speed;
		break;
	case BACKWARDS:
		this->currentSpeed = -this->turnDirection*this->properties.speed;
		break;
	case LEFT:
		this->currentSpeed = glm::rotate(glm::vec3(this->turnDirection.x, 0, this->turnDirection.z), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f))*this->properties.speed;
		break;
	case RIGHT:
		this->currentSpeed = glm::rotate(glm::vec3(this->turnDirection.x, 0, this->turnDirection.z), -90.0f, glm::vec3(0.0f, 1.0f, 0.0f))*this->properties.speed;
		break;
	case UP:
		this->currentSpeed = glm::vec3(0.0f, 1.0f, 0.0f)*this->properties.speed;
		break;
	case DOWN:
		this->currentSpeed = -glm::vec3(0.0f, 1.0f, 0.0f)*this->properties.speed;
		break;
	}
}

void Player::turn(float turnX, float turnY){
	glm::vec3 axisX = glm::vec3(0,1,0);
	glm::vec3 axisY = glm::cross(this->turnDirection, glm::vec3(0,1,0));
	this->turnDirection = glm::rotate(this->turnDirection, turnX*this->properties.angleSpeed, axisX);

	this->turnDirection = glm::rotate(this->turnDirection, turnY*this->properties.angleSpeed, axisY);
}

glm::vec3  Player::getPosition(){
	return this->position;
}

void Player::step(float dt){
	this->position += this->currentSpeed*dt;
	this->currentSpeed = glm::vec3(0);

}

float Player::getHeight(){
	return this->properties.height;
}

glm::vec3 Player::getCurrentTurn(){
	return this->turnDirection;
}
