/*
 * Camera.hpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr
#include <glm/gtx/vector_angle.hpp>

#include "Player.h"

class Camera{
	Player * player;
public:
	glm::vec3 getPosition();
	Camera(Player * player);

	glm::mat4 getViewMatrix();
};


#endif /* CAMERA_HPP_ */
