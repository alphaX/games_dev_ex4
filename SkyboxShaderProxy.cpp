/*
 * SkyboxShaderProxy.cpp
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#include "SkyboxShaderProxy.h"

SkyboxShaderProxy::SkyboxShaderProxy(CGcontext cgContext, const char * fxFilepath):ShaderProxy(cgContext, fxFilepath)  {
	vertexShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "skyBoxVS", 0);
	cgGLLoadProgram(vertexShader);
	pixelShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgFprofile, "skyBoxPS", 0);
	cgGLLoadProgram(pixelShader);

	if(!(wvp = cgGetNamedParameter(vertexShader, "wvp"))){
		printf("failed to get parameter wvp\n");
	}

	if(!(position = cgGetNamedParameter(vertexShader, "IN.position"))){
		printf("failed to get parameter position\n");
	}

	if(!(cubeSampler = cgGetNamedParameter(pixelShader, "sam"))){
			printf("failed to get parameter sam\n");
	}

	if(!(textureCoords = cgGetNamedParameter(vertexShader, "IN.textureCoords"))){
		printf("failed to get parameter textureCoords\n");
	}
}

void SkyboxShaderProxy::setTexture(GLuint tex){
	cgGLEnableTextureParameter( cubeSampler );
	cgGLSetupSampler( cubeSampler, tex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex); //Binding the texture
}

void SkyboxShaderProxy::setPosition(GLuint verticeBuffer, GLuint indexBuffer){
	glBindBuffer(GL_ARRAY_BUFFER, verticeBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	cgGLEnableClientState(position);
	cgGLSetParameterPointer(position, 3, GL_FLOAT, 0, 0);
}

void SkyboxShaderProxy::setTextureCoordinates(GLuint texCoordsBuffer){
	glBindBuffer(GL_ARRAY_BUFFER, texCoordsBuffer);
	cgGLEnableClientState(textureCoords);
	cgGLSetParameterPointer(textureCoords, 3, GL_FLOAT, 0, 0);
}

void SkyboxShaderProxy::enable(){
	cgGLBindProgram(this->vertexShader);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(this->pixelShader);
	cgGLEnableProfile(cgFprofile);
}

void SkyboxShaderProxy::setWvp(glm::mat4 wvp){
	cgGLSetMatrixParameterfc(this->wvp, glm::value_ptr(wvp));
}

void SkyboxShaderProxy::postRenderClean(){
	cgGLDisableClientState(position);
	cgGLDisableTextureParameter( cubeSampler );
	cgGLDisableClientState(textureCoords);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgFprofile);
}

SkyboxShaderProxy::~SkyboxShaderProxy() {
	// TODO Auto-generated destructor stub
}

