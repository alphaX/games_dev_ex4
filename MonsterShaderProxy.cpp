/*
 * MonsterShaderProxy.cpp
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#include "MonsterShaderProxy.h"


MonsterShaderProxy::MonsterShaderProxy(CGcontext cgContext,const char* fxFilepath): ShaderProxy(cgContext, fxFilepath) {
	feedbackVertexShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "particleFeedbackVP", 0);
	cgGLLoadProgram(feedbackVertexShader);

	feedbackGeoShade = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgGprofile, "particleFeedbackGP", 0);
	cgGLLoadProgram(feedbackGeoShade);

	displayVertexShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "particleDisplayVP", 0);
	cgGLLoadProgram(displayVertexShader);

	//displayGeoShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgGprofile, "particleDisplayGP", 0);
	//cgGLLoadProgram(displayGeoShader);

	displayFragShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgFprofile, "particleDisplayFP", 0);
	cgGLLoadProgram(displayFragShader);

	shaderWvp = getParameter(displayVertexShader, "wvp");
	feedbackShaderPosition = getParameter(feedbackVertexShader, "IN.pos");
	feedbackShaderRandomTexture = getParameter(feedbackGeoShade, "rand_tex");
	displayShaderPosition = getParameter(feedbackVertexShader, "IN.pos");
	feedbackShaderRandTransform = getParameter(feedbackGeoShade, "rand_xform");

}

MonsterShaderProxy::~MonsterShaderProxy() {
	// TODO Auto-generated destructor stub
}

void MonsterShaderProxy::setWvp(glm::mat4 wvp) {
	cgGLSetMatrixParameterfc(this->shaderWvp, glm::value_ptr(wvp));
}

void MonsterShaderProxy::setPositionForFeedback(GLuint vertexBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	cgGLEnableClientState(feedbackShaderPosition);
	cgGLSetParameterPointer(feedbackShaderPosition, 4, GL_FLOAT, 0, 0);
}

void MonsterShaderProxy::setPositionForDisplay(GLuint vertexBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	cgGLEnableClientState(displayShaderPosition);
	cgGLSetParameterPointer(displayShaderPosition, 4, GL_FLOAT, 0, 0);
}

void MonsterShaderProxy::setRandomTexture(GLuint randTex) {
	cgGLEnableTextureParameter( feedbackShaderRandomTexture );
	cgGLSetupSampler( feedbackShaderRandomTexture, randTex);
	glBindTexture(GL_TEXTURE_2D, randTex); //Binding the texture
}

void MonsterShaderProxy::enableGeneration() {
	cgGLBindProgram(this->feedbackVertexShader);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(this->feedbackGeoShade);
	cgGLEnableProfile(cgGprofile);
}

void MonsterShaderProxy::enableDisplay() {
	cgGLBindProgram(this->displayVertexShader);
	cgGLEnableProfile(cgVprofile);

	//cgGLBindProgram(this->displayGeoShader);
	//cgGLEnableProfile(cgGprofile);

	cgGLBindProgram(displayFragShader);
	cgGLEnableProfile(cgFprofile);
}

void MonsterShaderProxy::postGenerationClean() {
	cgGLDisableTextureParameter(feedbackShaderRandomTexture);
	cgGLDisableClientState(feedbackShaderPosition);
	//cgGLDisableClientState(feedbackShaderRandTransform);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgGprofile);
}

void MonsterShaderProxy::setRandTransform(float scale, float offset) {
	cgGLSetParameter2f(feedbackShaderRandTransform, scale, offset);
}

void MonsterShaderProxy::postDisplayClean() {
	cgGLDisableClientState(displayShaderPosition);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgGprofile);
	cgGLDisableProfile(cgFprofile);
}
