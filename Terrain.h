/*
 * Terrain.h
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#ifndef TERRAIN_H_
#define TERRAIN_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include "Texture2D.h"
#include "BufferedEntity.h"

class Terrain:public BufferedEntity {
public:
	Terrain(GLuint buffer, int numberOfVertexes);
	virtual ~Terrain();

	void setTextures(Texture2D & level1, Texture2D & level2);
	GLuint getLevel1Texture();
	GLuint getLevel2Texture();

	float getHeightAtPoint(glm::vec2 point);
private:
	GLuint level1Texture, level2Texture;
};

#endif /* TERRAIN_H_ */
