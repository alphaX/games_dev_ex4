/*
 * TreeGenerator.cpp
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#include "TreeGenerator.h"
#include "RandomUtils.h"
#include "RandomTextureGenerator.h"

TreeGenerator::TreeGenerator(TreeShaderProxy & shaderProxy) :
		shaderProxy(shaderProxy), vertexesPerPrimitive(2), primOutputDiv(1) {
	// TODO Auto-generated constructor stub
	currentBuffer = 0;
	primitivesWritten = 1;
	heightRandomScale = 1;
}

TreeGenerator::~TreeGenerator() {
	// TODO Auto-generated destructor stub
}

Tree TreeGenerator::generateTree(int steps) {

	GLint attribs[] = { GL_POSITION, 4, 0 };
	glTransformFeedbackAttribsNV(1, attribs, GL_SEPARATE_ATTRIBS_NV);
	generateBase();
	randomTexture = RandomTextureGenerator::generateWhiteNoise(256, 256);

	for(int i=0; i< steps; i++)
		feedbackIteration();

	inflateTree();

	Texture2D treeTexture("textures/tree/tree.tga");
	Tree * tree = new Tree(vertexBuffer[currentBuffer],
			primitivesWritten * 1/ primOutputDiv);
	tree->setWorldMatrix(
			glm::scale(
					glm::translate(glm::mat4(1.0f), glm::vec3(0,47,0))
				, glm::vec3(2, 2,2)));
	tree->setTexture(treeTexture);
	return *tree;
}

void TreeGenerator::feedbackIteration() {
	printf("TREE: feedback start. generated primitive: %d\n", primitivesWritten);
	GLuint query;
	glGenQueries(1, &query);

	// set base for transform feedback
	glBindBufferOffsetNV(GL_TRANSFORM_FEEDBACK_BUFFER_NV, 0,
			vertexBuffer[1 - currentBuffer], 0);

	glBeginTransformFeedbackNV(GL_LINES);
	glEnable(GL_RASTERIZER_DISCARD_NV);    // disable rasterization

	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV, query);

	shaderProxy.enableGeneration();
	shaderProxy.setRandomTexture(randomTexture);
	shaderProxy.setPositionForGeneration(vertexBuffer[currentBuffer]);
	shaderProxy.setHeightRandScale(heightRandomScale);
	shaderProxy.setRandomTexture(randomTexture);
	//shaderProxy.setTransformRandScale(RandomUtils::frand(), RandomUtils::frand());
	shaderProxy.setTransformRandScale(0.5, 0.5);
	glDrawArrays(GL_LINES, 0,
			primitivesWritten * vertexesPerPrimitive / primOutputDiv);

	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV);
	shaderProxy.postGenerationClean();
	glDisable(GL_RASTERIZER_DISCARD_NV);
	glEndTransformFeedbackNV();

	// read back query results
	glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitivesWritten);
	printf("TREE: feedback end. generated primitive: %d\n", primitivesWritten);

	currentBuffer = 1 - currentBuffer;
	heightRandomScale *= 0.5;
}

void TreeGenerator::generateBase() {
	int max_buffer_verts = 1<<20;
	glGenBuffers(2, vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, max_buffer_verts*4*sizeof(GLfloat), 0, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[1]);
	glBufferData(GL_ARRAY_BUFFER, max_buffer_verts*4*sizeof(GLfloat), 0, GL_STATIC_DRAW);

	GLfloat v[] = {
			0.0, 0, 0.0, 1.0,
			0.0, 1, 0.0, 1.0,
	};

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[currentBuffer]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * 2 * sizeof(GLfloat), v);
}

void TreeGenerator::inflateTree() {
	printf("TREE: inflate start. generated primitive: %d\n", primitivesWritten);
	GLuint query;
	glGenQueries(1, &query);

	// set base for transform feedback
	glBindBufferOffsetNV(GL_TRANSFORM_FEEDBACK_BUFFER_NV, 0,
			vertexBuffer[1 - currentBuffer], 0);

	glBeginTransformFeedbackNV(GL_POINTS);
	glEnable(GL_RASTERIZER_DISCARD_NV);    // disable rasterization

	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV, query);

	shaderProxy.enableInflate();
	shaderProxy.setPositionForGeneration(vertexBuffer[currentBuffer]);

	glDrawArrays(GL_LINES, 0,
			primitivesWritten * vertexesPerPrimitive / primOutputDiv);

	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV);
	shaderProxy.postInflateClean();
	glDisable(GL_RASTERIZER_DISCARD_NV);
	glEndTransformFeedbackNV();

	// read back query results
	glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitivesWritten);
	printf("TREE: inflate end. generated primitive: %d\n", primitivesWritten);

	currentBuffer = 1 - currentBuffer;
}
