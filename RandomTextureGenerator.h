/*
 * RandomTextureGenerator.h
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#ifndef RANDOMTEXTUREGENERATOR_H_
#define RANDOMTEXTUREGENERATOR_H_

#include <GL/glew.h>
#include <GL/glfw.h>

class RandomTextureGenerator {
public:
	RandomTextureGenerator();
	static GLuint generateWhiteNoise(int w, int h);
	virtual ~RandomTextureGenerator();
};

#endif /* RANDOMTEXTUREGENERATOR_H_ */
