/*
 * GLRenderer.h
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#ifndef GLRENDERER_H_
#define GLRENDERER_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>


class GLRenderer {
public:
	GLRenderer();
	virtual ~GLRenderer();

	void setProjection(glm::mat4 projection);
protected:
	glm::mat4 projection;
};

#endif /* GLRENDERER_H_ */
