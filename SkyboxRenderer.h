/*
 * SkyBoxRenderer.h
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#ifndef SKYBOXRENDERER_H_
#define SKYBOXRENDERER_H_


#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include "GLRenderer.h"
#include "SkyboxShaderProxy.h"
#include "Entity.h"
#include "Camera.h"

class SkyboxRenderer: public GLRenderer {
public:
	SkyboxRenderer(Entity & entity, Camera & camera, SkyboxShaderProxy * shaderProxy);
	void render();
	virtual ~SkyboxRenderer();
private:
	Entity & entity;
	Camera & camera;

	SkyboxShaderProxy * shaderProxy;

	GLuint verticeBuffer;
	GLuint indexBuffer;
	GLuint textureCoordsBuffer;

	void bufferVertices();
	void bufferTexture();
};

#endif /* SKYBOXRENDERER_H_ */
