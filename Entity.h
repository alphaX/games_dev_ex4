/*
 * Entity.h
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include "Mesh.h"
#include "CubeTexture.h"

#include <vector>

class Entity {
public:
	Entity(Mesh & mesh);
	virtual ~Entity();

	void setWorldMatrix(glm::mat4);
	glm::mat4 getWorldMatrix();
	Mesh & getMesh();
	std::vector<glm::vec3> & getTextureCoords();
	GLuint getTextureBuffer();
	CubeTexture & getTexture();
	void applyTexture(CubeTexture * texture, std::vector<glm::vec3> texCoords);
	bool hasTexture();
private:
	glm::mat4 world;
	Mesh & mesh;
	CubeTexture * texture;
	std::vector<glm::vec3> textureCoords;
};

#endif /* ENTITY_H_ */
