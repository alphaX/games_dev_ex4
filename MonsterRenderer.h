/*
 * MonsterRenderer.h
 *
 *  Created on: May 14, 2013
 *      Author: alex
 */

#ifndef MONSTERRENDERER_H_
#define MONSTERRENDERER_H_


#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include "GLRenderer.h"
#include "Camera.h"
#include "Monster.h"
#include "MonsterShaderProxy.h"

class MonsterRenderer: public GLRenderer {
public:
	MonsterRenderer(Monster & m,Camera & c, MonsterShaderProxy shaderProxy);
	virtual ~MonsterRenderer();

	void render();

private:
	Monster & monster;
	Camera & camera;
	MonsterShaderProxy shaderProxy;

	GLuint vertexBuffer;
};

#endif /* MONSTERRENDERER_H_ */
