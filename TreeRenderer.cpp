/*
 * TreeRenderer.cpp
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#include "TreeRenderer.h"

TreeRenderer::TreeRenderer(Tree & tree, Camera & c, TreeShaderProxy & shaderProxy): tree(tree), camera(c), shaderProxy(shaderProxy) {
	// TODO Auto-generated constructor stub

}

TreeRenderer::~TreeRenderer() {
	// TODO Auto-generated destructor stub
}

void TreeRenderer::render() {
	glBindBuffer(GL_ARRAY_BUFFER, tree.getBuffer());

	glm::mat4 world = tree.getWorldMatrix();
	glm::mat4 view = camera.getViewMatrix();
	glm::mat4 wvp = projection * view * world;
	glm::mat4 w =  world;
	shaderProxy.enableDisplay();
	shaderProxy.setWvp(wvp);
	shaderProxy.setWv(w);
	shaderProxy.setPositionForDisplay(tree.getBuffer());
	shaderProxy.setTreeTexutre(tree.getTexture());

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawArrays(GL_LINES_ADJACENCY_EXT, 0, tree.getNumberOfVertexes());
	//glDrawArrays(GL_POINTS, 0, tree.getNumberOfVertexes());


	shaderProxy.postDisplayClean();
}
