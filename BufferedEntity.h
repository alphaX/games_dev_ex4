/*
 * BufferdEntity.h
 *
 *  Created on: May 17, 2013
 *      Author: alex
 */

#ifndef BUFFERDENTITY_H_
#define BUFFERDENTITY_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

class BufferedEntity {
public:
	BufferedEntity(GLuint buffer, int numberOfVertexes);
	virtual ~BufferedEntity();

	void setWorldMatrix(glm::mat4 world);
	glm::mat4 getWorldMatrix();
	int getNumberOfVertexes();
	GLuint getBuffer();
private:
	GLuint buffer;
	glm::mat4 world;
	int numberOfVertexes;

};

#endif /* BUFFERDENTITY_H_ */
