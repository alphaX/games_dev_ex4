/*
 * ShaderProxy.cpp
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#include "ShaderProxy.h"

#include "DebugShaderProxy.h"
#include "Entity.h"
#include "Camera.h"

ShaderProxy::ShaderProxy(CGcontext cgContext,const char * fxFilepath) {
	this->cgContext = cgContext;
	this->fxFilepath = fxFilepath;
	cgVprofile = cgGLGetLatestProfile(CG_GL_VERTEX);
	cgFprofile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
	cgGprofile = cgGLGetLatestProfile(CG_GL_GEOMETRY);
}

ShaderProxy::~ShaderProxy() {
	// TODO Auto-generated destructor stub
}

CGparameter ShaderProxy::getParameter(CGprogram & program, const char* name) {
	CGparameter parameter = cgGetNamedParameter(program, name);
	if(parameter == NULL){
		printf("%s parameter of file %s is invalid ", name, fxFilepath);
		exit(0);
	}
	return parameter;
}
