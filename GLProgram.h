/*
 * GLProgram.h
 *
 *  Created on: Apr 26, 2013
 *      Author: alex
 */

#ifndef GLPROGRAM_H_
#define GLPROGRAM_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <Cg/cgGL.h>

class GLProgram {
public:
	GLProgram(const char * title);
	virtual ~GLProgram();

	CGcontext getContext();
	int getWinHeight();
	int getWinWidth();
private:
	CGcontext cgContext;

	void initGL(const char * title);
	void initCG();
	int winHeight;
	int winWidth;
};

#endif /* GLPROGRAM_H_ */
