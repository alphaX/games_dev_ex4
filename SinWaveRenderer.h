/*
 * SinWaveRenderer.h
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#ifndef SINWAVERENDERER_H_
#define SINWAVERENDERER_H_
#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include "GLRenderer.h"
#include "SinWaveShaderProxy.h"
#include "Entity.h"
#include "Camera.h"
#include "Clock.h"

class SinWaveRenderer: public GLRenderer {
public:
	SinWaveRenderer(Entity & entity, Camera & camera, Clock & clock, CubeTexture & envTex, SinWaveShaderProxy * shaderProxy);
	virtual ~SinWaveRenderer();
	void render();
private:
	Entity & entity;
	Camera & camera;
	SinWaveShaderProxy * shaderProxy;
	Clock & clock;
	CubeTexture & envTexture;
	GLuint verticeBuffer;
	GLuint indexBuffer;
	GLuint textureCoordsBuffer;

	void bufferVertices();
	void bufferTexture();
};

#endif /* SINWAVERENDERER_H_ */
