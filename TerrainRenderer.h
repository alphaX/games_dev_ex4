/*
 * TerrainRenderer.h
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#ifndef TERRAINRENDERER_H_
#define TERRAINRENDERER_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include "GLRenderer.h"
#include "Terrain.h"
#include "Camera.h"
#include "TerrainShaderProxy.h"

class TerrainRenderer: public GLRenderer {
public:
	TerrainRenderer(Terrain & t, Camera & c, TerrainShaderProxy & shaderProxy);
	void render();
	virtual ~TerrainRenderer();
private:
	Terrain & terrain;
	Camera & camera;
	TerrainShaderProxy & shaderProxy;
};

#endif /* TERRAINRENDERER_H_ */
