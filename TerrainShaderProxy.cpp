/*
 * TerrainShaderProxy.cpp
 *
 *  Created on: May 10, 2013
 *      Author: alex
 */

#include "TerrainShaderProxy.h"

TerrainShaderProxy::TerrainShaderProxy(CGcontext cgContext,const char * fxFilepath): ShaderProxy(cgContext, fxFilepath) {
	passthruVertexShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "passthru_vp", 0);
	cgGLLoadProgram(passthruVertexShader);

	transformVertexShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "transform_vp", 0);
	cgGLLoadProgram(transformVertexShader);

	generationShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgGprofile, "generation_gp", 0);
	cgGLLoadProgram(generationShader);

	terrainDisplayShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgGprofile, "terrain_display_gp", 0);
	cgGLLoadProgram(terrainDisplayShader);

	displayFragmentShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgFprofile, "terrain_display_fp", 0);
	cgGLLoadProgram(displayFragmentShader);

	passthruShaderPosition = cgGetNamedParameter(passthruVertexShader, "pos");

	transformShaderPosition = cgGetNamedParameter(transformVertexShader, "pos");
	shaderWVP = cgGetNamedParameter(transformVertexShader, "wvp");
	shaderWV = cgGetNamedParameter(transformVertexShader, "wv");

	shaderRandomTexture = cgGetNamedParameter(generationShader, "rand_tex");
	shaderHeightRandScale = cgGetNamedParameter(generationShader, "rand_scale");
	shaderTransformRandScale = cgGetNamedParameter(generationShader, "rand_xform");

	level1Texture = getParameter(displayFragmentShader, "level1Tex");
	level2Texture = getParameter(displayFragmentShader, "level2Tex");

	cameraLocation = getParameter(terrainDisplayShader, "cameraLocation");
}

void TerrainShaderProxy::setWvp(glm::mat4 wvp) {
	cgGLSetMatrixParameterfc(this->shaderWVP, glm::value_ptr(wvp));
}

void TerrainShaderProxy::setWv(glm::mat4 wv) {
	cgGLSetMatrixParameterfc(this->shaderWV, glm::value_ptr(wv));
}

void TerrainShaderProxy::setPositionForGeneration(GLuint vertexBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	cgGLEnableClientState(passthruShaderPosition);
	cgGLSetParameterPointer(passthruShaderPosition, 4, GL_FLOAT, 0, 0);
}

void TerrainShaderProxy::setPositionForDisplay(GLuint vertexBuffer) {
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	cgGLEnableClientState(transformShaderPosition);
	cgGLSetParameterPointer(transformShaderPosition, 4, GL_FLOAT, 0, 0);
}


void TerrainShaderProxy::enableGeneration() {
	cgGLBindProgram(this->passthruVertexShader);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(this->generationShader);
	cgGLEnableProfile(cgGprofile);
}

void TerrainShaderProxy::enableDisplay() {
	cgGLBindProgram(this->transformVertexShader);
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(this->terrainDisplayShader);
	cgGLEnableProfile(cgGprofile);

	cgGLBindProgram(this->displayFragmentShader);
	cgGLEnableProfile(cgFprofile);
}

void TerrainShaderProxy::setTexutres(GLuint l1, GLuint l2) {
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, l1); //Binding the texture
	cgGLEnableTextureParameter( level1Texture );
	cgGLSetupSampler( level1Texture, l1);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, l2); //Binding the texture
	cgGLEnableTextureParameter( level2Texture );
	cgGLSetupSampler( level2Texture, l2);
}

void TerrainShaderProxy::postDisplayClean() {
	cgGLDisableClientState(transformShaderPosition);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgGprofile);
	cgGLDisableTextureParameter( level1Texture );
	cgGLDisableTextureParameter( level2Texture );

	//cgGLDisableProfile(cgFprofile);
}

void TerrainShaderProxy::postGenerationClean() {
	cgGLDisableClientState(passthruShaderPosition);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgGprofile);
}

void TerrainShaderProxy::setHeightRandScale(float scale) {
	cgGLSetParameter1f(shaderHeightRandScale, scale);
}

void TerrainShaderProxy::setTransformRandScale(float scale, float offset) {
	cgGLSetParameter2f(shaderTransformRandScale, scale, offset);
}

void TerrainShaderProxy::setRandomTexture(GLuint randTex) {
	cgGLEnableTextureParameter( shaderRandomTexture );
	cgGLSetupSampler( shaderRandomTexture, randTex);
	glBindTexture(GL_TEXTURE_2D, randTex); //Binding the texture
}

TerrainShaderProxy::~TerrainShaderProxy() {
	// TODO Auto-generated destructor stub
}

void TerrainShaderProxy::setCameraLocation(glm::vec3 cameraLocation) {
	cgGLSetParameter3f(this->cameraLocation, cameraLocation.x, cameraLocation.y, cameraLocation.z);
}
