/*
 * MonsterShaderProxy.h
 *
 *  Created on: May 11, 2013
 *      Author: alex
 */

#ifndef MONSTERSHADERPROXY_H_
#define MONSTERSHADERPROXY_H_
#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr
#include "ShaderProxy.h"


class MonsterShaderProxy: public ShaderProxy {
public:
	MonsterShaderProxy(CGcontext cgContext, const char * fxFilepath);
	virtual ~MonsterShaderProxy();
	void setWvp(glm::mat4 wvp);
	void setPositionForFeedback(GLuint vertexBuffer);
	void setPositionForDisplay(GLuint vertexBuffer);
	void setRandomTexture(GLuint randTex);
	void setRandTransform(float scale, float offset);
	void enableGeneration();
	void enableDisplay();
	void postGenerationClean();
	void postDisplayClean();
private:
	CGprogram feedbackVertexShader, feedbackGeoShade, displayVertexShader, displayGeoShader, displayFragShader;
	CGparameter shaderWvp, feedbackShaderPosition, feedbackShaderRandTransform, displayShaderPosition, feedbackShaderRandomTexture;
};

#endif /* MONSTERSHADERPROXY_H_ */
