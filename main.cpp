#include <vector>
#include <unistd.h>

#include "GLProgram.h"
#include "Mesh.h"
#include "DebugShaderProxy.h"
#include "DebugRenderer.h"
#include <glm/glm.hpp>
#include "Player.h"
#include "Camera.h"
#include "Controles.h"
#include "CubeTexture.h"
#include "EntityFactory.h"
#include "SkyboxRenderer.h"
#include "SinWaveRenderer.h"
#include "Clock.h"
#include "Terrain.h"
#include "TerrainGenerator.h"
#include "TerrainRenderer.h"
#include "TerrainShaderProxy.h"

#include "MonsterShaderProxy.h"
#include "Monster.h"
#include "MonsterRenderer.h"

#include "Tree.h"
#include "TreeShaderProxy.h"
#include "TreeGenerator.h"
#include "TreeRenderer.h"
#include <time.h>

void updateFPSCounter(float dt){
	static char fps[255];
	static float nFrames = 0;
	static float t = 0.0f;
	nFrames++;
	t += dt;
	if( t >= 1.0) {
		printf("FPS: %f fps", nFrames/t);
		nFrames = 0.0f;
		t = 0;
	}
}


int main( int argc, char** argv ){
	srand (time(NULL));
	printf("enter program\n");
	fflush(stdout);

	GLProgram glProgram("Ex4");
	printf("configured program\n");
	fflush(stdout);

	Clock clock;

	PlayerPropeties playerProperties;
	playerProperties.speed = 20; // units per second
	playerProperties.height = 0;
	playerProperties.angleSpeed = 0.1;

	Player player(playerProperties);
	Camera camera(&player);

	Controles controles(player);

	Entity skybox = EntityFactory::createSkybox();
	Entity plane = EntityFactory::createPlane(400, 400, 0.5, 0.5);

	printf("created skybox\n");

	DebugShaderProxy debugProxy(glProgram.getContext(), "debug.cgfx");
	SkyboxShaderProxy skyboxProxy(glProgram.getContext(), "cgSkyBox.cgfx");
	SinWaveShaderProxy sinWaveProxy(glProgram.getContext(), "cgSinWaves.cgfx");

	printf("created debug proxy\n");

	glm::mat4 projection = perspective(45.0f,(float)glProgram.getWinWidth() / glProgram.getWinHeight(), 0.1f, 300.f);
	SkyboxRenderer skyboxRenderer(skybox,camera, &skyboxProxy);
	skyboxRenderer.setProjection(projection);
	printf("defined skybox\n");

	SinWaveRenderer waveRenderer(plane, camera, clock, skybox.getTexture(), &sinWaveProxy);
	waveRenderer.setProjection(projection);
	printf("defined wave\n");


	TerrainShaderProxy terrainShaderProxy(glProgram.getContext(), "cgTerrain.cgfx");
	printf("generated terrain shader proxy\n");

	TerrainGenerator terrainGenerator(terrainShaderProxy);
	Terrain terrain = terrainGenerator.generateTerrain(7);

	//printf("generated terrain. number of generated vertexes: %d\n", terrain.getNumberOfVertexes());

	TerrainRenderer terrainRenderer(terrain, camera, terrainShaderProxy);
	terrainRenderer.setProjection(projection);
	printf("defined terrain\n");

	MonsterShaderProxy monsterShaderProxy(glProgram.getContext(), "cgMonster.cgfx");
	Monster monster(monsterShaderProxy);
	monster.setPosition(glm::vec3(-20,45,0));
	//monster.setWorldMatrix(glm::translate(glm::mat4(1), glm::vec3(0,15,0)));

	MonsterRenderer monsterRenderer(monster, camera, monsterShaderProxy);
	monsterRenderer.setProjection(projection);
	printf("defined monster\n");

	TreeShaderProxy treeShaderProxy(glProgram.getContext(), "cgTree.cgfx");
	TreeGenerator treeGenerator(treeShaderProxy);
	Tree tree = treeGenerator.generateTree(5);
	TreeRenderer treeRenderer(tree, camera, treeShaderProxy);
	treeRenderer.setProjection(projection);

	printf("start render loop\n");
	do {
		clock.update();
		float dt = clock.getDt();
		//printf("fps: %f", 1/clock.getDt());
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.3f, 0.1f, 0.1f, 0.0f);
		controles.handleInput();
		player.step(dt);
		skyboxRenderer.render();

		waveRenderer.render();
		terrainRenderer.render();

		monster.update(dt);
		monsterRenderer.render();

		treeRenderer.render();

		glfwSwapBuffers();
	} while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS && glfwGetWindowParam( GLFW_OPENED ) );
	printf("exiting");
}
