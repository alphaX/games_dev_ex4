/*
 * SinWaveShaderProxy.h
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#ifndef SINWAVESHADERPROXY_H_
#define SINWAVESHADERPROXY_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr
#include "ShaderProxy.h"

class SinWaveShaderProxy: public ShaderProxy {
public:
	SinWaveShaderProxy(CGcontext cgContext,const char * fxFilepath);
	virtual ~SinWaveShaderProxy();

	void setWvp(glm::mat4 wvp);
	void setPositionWithCurrentBuffers();
	void setTime(double time);
	void setWorldMatrix(glm::mat4 world);
	void setEyePos(glm::vec3 eyePos);
	void setTexture(GLuint tex);
	void preRender();
	void postRenderClean();

private:
	CGprogram vertexShader;
	CGprogram pixelShader;
	CGparameter cgWvp;
	CGparameter cgPosition;
	CGparameter cgTime;
	CGparameter cgWorld;
	CGparameter cgEyePos;
	CGparameter cgEnvSamper;
};

#endif /* SINWAVESHADERPROXY_H_ */
