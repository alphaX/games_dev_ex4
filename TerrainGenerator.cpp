/*
 * TerrainGenerator.cpp
 *
 *  Created on: May 10, 2013
 *      Author: alex
 */

#include "TerrainGenerator.h"
#include "RandomTextureGenerator.h"
#include "RandomUtils.h"
#include "Texture2D.h"

TerrainGenerator::TerrainGenerator(TerrainShaderProxy & shaderProxy) :
		shaderProxy(shaderProxy), vertexesPerPrimitive(4), primOutputDiv(4) {
	// TODO Auto-generated constructor stub
	currentBuffer = 0;
	primitivesWritten = 4;
	heightRandomScale = 1;
}

TerrainGenerator::~TerrainGenerator() {
	// TODO Auto-generated destructor stub
}

Terrain TerrainGenerator::generateTerrain(int steps) {

	GLint attribs[] = { GL_POSITION, 4, 0 };
	glTransformFeedbackAttribsNV(1, attribs, GL_SEPARATE_ATTRIBS_NV);
	generateBase();
	randomTexture = RandomTextureGenerator::generateWhiteNoise(256, 256);

	for(int i=0; i< steps; i++)
		feedbackIteration();


	Terrain * terrain = new Terrain(vertexBuffer[currentBuffer],
			primitivesWritten * vertexesPerPrimitive/ primOutputDiv);
	terrain->setWorldMatrix(
			glm::scale(
					//glm::mat4(1.0)//
					glm::translate(glm::mat4(1.0f), glm::vec3(0,-3,0))
				, glm::vec3(100, 50,100)));

	Texture2D sand = Texture2D("textures/terrain/sand1.tga");
	Texture2D grass = Texture2D("textures/terrain/grass1.tga");
	terrain->setTextures(sand, grass);
	return *terrain;
}

void TerrainGenerator::feedbackIteration() {
	printf("feedback start. generated primitive: %d\n", primitivesWritten * vertexesPerPrimitive / primOutputDiv);
	GLuint query;
	glGenQueries(1, &query);

	// set base for transform feedback
	glBindBufferOffsetNV(GL_TRANSFORM_FEEDBACK_BUFFER_NV, 0,
			vertexBuffer[1 - currentBuffer], 0);

	glBeginTransformFeedbackNV(GL_POINTS);
	glEnable(GL_RASTERIZER_DISCARD_NV);    // disable rasterization

	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV, query);

	shaderProxy.enableGeneration();
	shaderProxy.setRandomTexture(randomTexture);
	shaderProxy.setPositionForGeneration(vertexBuffer[currentBuffer]);
	shaderProxy.setHeightRandScale(heightRandomScale);

	glDrawArrays(GL_LINES_ADJACENCY_EXT, 0,
			primitivesWritten * vertexesPerPrimitive / primOutputDiv);

	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV);
	shaderProxy.postGenerationClean();
	glDisable(GL_RASTERIZER_DISCARD_NV);
	glEndTransformFeedbackNV();

	// read back query results
	glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitivesWritten);
	printf("feedback end. generated primitive: %d\n", primitivesWritten);

	currentBuffer = 1 - currentBuffer;
	heightRandomScale *= 0.5;
}

//Terrain TerrainGenerator::generateSimplexTerrain(int w, int h, float gridDensityX, float gridDensityZ ) {
//	//Entity plane = EntityFactory::createPlane(w, h, gridDensityX, gridDensityZ);
//
//}

void TerrainGenerator::generateBase() {
	int max_buffer_verts = 1<<20;
	glGenBuffers(2, vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[0]);
	glBufferData(GL_ARRAY_BUFFER, max_buffer_verts*4*sizeof(GLfloat), 0, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[1]);
	glBufferData(GL_ARRAY_BUFFER, max_buffer_verts*4*sizeof(GLfloat), 0, GL_STATIC_DRAW);
	GLfloat h[4];
	for (int i = 0; i < 4; i++)
		h[i] = 0;//RandomUtils::frand();

	GLfloat v[] = {
			-1.0, h[0], -1.0, 1.0,
			 1.0, h[1], -1.0, 1.0,
			 1.0, h[2], 1.0, 1.0,
			-1.0, h[3], 1.0, 1.0,
	};

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer[currentBuffer]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * 4 * sizeof(GLfloat), v);
}
