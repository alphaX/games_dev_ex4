/*
 * EntityFactory.h
 *
 *  Created on: May 4, 2013
 *      Author: alex
 */

#ifndef ENTITYFACTORY_H_
#define ENTITYFACTORY_H_

#include "Entity.h"
#include <glm/glm.hpp>

using namespace glm;
using namespace std;

class EntityFactory {
public:
	EntityFactory();
	static Entity createSkybox();
	static Entity createPlane(int gridSizeX, int gridSizeZ, float gridDensityX, float gridDensityZ);
	virtual ~EntityFactory();
};

#endif /* ENTITYFACTORY_H_ */
