/********************************
* Terrain Generation
********************************/

float4 feedbackVS(
    float4 pos : POSITION 
    ) : POSITION
{
    return pos;
}

float4 rotate(float4 vec, float degreeX, float degreeY, float degreeZ){
	float4x4 rx = float4x4 (
    	1,	0				,0,		   	  0,
    	0, 	cos(degreeX),	-sin(degreeX),0,
    	0, 	sin(degreeX),	cos(degreeX), 0,
    	0,	0,				0,			  1
    	);
    
    float4x4 ry = float4x4 (
    	cos(degreeY),	0,	sin(degreeY),	0,
    	0, 				1,	0,				0,
    	-sin(degreeY),	0,	cos(degreeY), 	0,
    	0,				0,				0,	1
	);
	
    float4x4 rz = float4x4 (
    	cos(degreeZ),	-sin(degreeZ),	0,	0,
    	sin(degreeZ), 	cos(degreeZ),	0,	0,
    	0, 				0,				1, 	0,
    	0,				0,				0,	1
	);
    		
    return mul(rx, mul(ry, mul(rz, vec)));
    //return mul(ry, mul(rx, vec));
}

float2 transformSeed(float2 seed, float2 transformRand){
	return seed*transformRand.x + transformRand.y;
}

float4 randRange(sampler2D randTex, float2 seed, float from, float to){
	float4 rand = (tex2D(randTex, seed)+1)/2;
	return rand*(to-from) + from;
}

/*
    Branching L-system

    |    \ /
    | ->  |
    |     |
*/
LINE
LINE_OUT
void feedbackGS(
	AttribArray<float4> pos : POSITION,
	uniform sampler2D randomTex,
	uniform float2 transformRand
	)
{
	float4 rand1 = randRange(randomTex, transformSeed(pos[0].xy, transformRand), 0,180);
	float4 rand2 = randRange(randomTex, transformSeed(pos[1].yz, transformRand), 30,60);
	float4 rand3 = randRange(randomTex, transformSeed(pos[0].yz, transformRand), 0.4,0.8);
	
	float y1 = (rand1.x);
	float y2 = (rand1.y+120);
	float y3 = (rand1.z+240);
	
	float z1 = radians(rand2.x);
	float z2 = radians(rand2.y);
	float z3 = radians(rand2.z);
	
	
    float4 diff = pos[1] - pos[0];
       
    float l = 1.6;
    float4 b1 = rotate(diff, 0, y1, z1)/l;
    float4 b2 = rotate(diff, 0, y2, z2)/l;
    float4 b3 = rotate(diff, 0, y3, z3)/l;
    
     // trunk
    emitVertex(pos[0] : POSITION);
    emitVertex(pos[1]  : POSITION);
    restartStrip();
    
    // branches
    emitVertex(pos[1] : POSITION);    
    emitVertex(pos[1] + b1 : POSITION);
    restartStrip();

	float4 p1 = float4(pos[0] + diff.xyz*rand3.x, 1);
    emitVertex(p1 : POSITION);    
    emitVertex(p1 + b2 : POSITION);
    restartStrip();
    
    float4 p2 = float4(pos[0] + diff.xyz*rand3.y, 1);
    emitVertex(p2 : POSITION);    
    emitVertex(p2 + b3 : POSITION);
    restartStrip();
}


LINE
POINT_OUT
void inflateFeedbackGP(
    AttribArray<float4> p : POSITION
    )
{	
	float4 b = float4(p[0]);
	float4 t = float4(p[1]);
	
	float3 branch = t.xyz-b.xyz;
	float l = length(branch);
	float EPSILON = 0.01;
	float3 e1 = float3(1,0,0), e2 = float3(0,0,1);
	
	if(abs(dot(normalize(abs(branch)), e2) - 1)  < EPSILON){
		e2 = float3(0,1,0);
	}
	
	if(abs(dot(normalize(abs(branch)), e1) - 1)  < EPSILON){
		e1 = float3(0,1,0);
	}
	
	float4 v[8];
	 	
	float width = l/20;
	int a0 = 0, a1 = 2, a2 = 4, a3 = 6, a01 = 1, a12 = 3, a23 = 5, a30 = 7;
	v[a0] = float4(normalize(cross(branch, e1))*width, 0);
	v[a1] = float4(normalize(cross(branch, e2))*width, 0);  
	v[a2] = -v[a0];
	v[a3] = -v[a1];
	
	v[a01] = normalize(v[a0]+v[a1])*width;
	v[a12] = normalize(v[a1]+v[a2])*width;
	v[a23] = normalize(v[a2]+v[a3])*width;
	v[a30] = normalize(v[a3]+v[a0])*width;
	
	float widthRatio = 1.6;
	for(int i=0; i<8;i++){
		int c = (i + 1)  % 8;
		int p = i % 8;
		emitVertex(t + v[c] : POSITION);
		emitVertex(t + v[p] : POSITION);
		emitVertex(b + v[p]*widthRatio  : POSITION);
		emitVertex(b + v[c]*widthRatio : POSITION);
	}
}
/*****************************
* Tree display
*****************************/
// calculate triangle normal
float3 calcNormal(float3 v0, float3 v1, float3 v2)
{
    float3 edge0 = v1 - v0;
    float3 edge1 = v2 - v0;
    return normalize(cross(edge0, edge1));
}

struct DisplayVPOutput{
	float4 pos: POSITION;
	float4 posW: TEXCOORD1; // position in world coords
};

DisplayVPOutput displayVp(
    float4 pos : POSITION,
    uniform float4x4 wvp,
    uniform float4x4 w
    )
{
	DisplayVPOutput OUT;
    OUT.pos = mul(wvp, pos);
    OUT.posW = mul(w, pos);
    
    return OUT;
}



LINE_ADJ    // really quad
TRIANGLE_OUT
void displayGP(
    AttribArray<float4> p : POSITION,
    AttribArray<float4> pw : TEXCOORD1,
    uniform float3 lightDir = { 0, 0, -1 }
    )
{
    float3 n = calcNormal(pw[0], pw[1], pw[3]);
    float col = dot(n, lightDir);
    float3 n2 = calcNormal(pw[1], pw[2], pw[3]);
    float col2 = dot(n2, lightDir);
    
    float2 uv0 = float2(0,0);
    float2 uv1 = float2(0,1);
    float2 uv2 = float2(1,1);
    float2 uv3 = float2(1,0);
    
    emitVertex(p[0] : POSITION, n : TEXCOORD2, uv0: TEXCOORD0, pw[0]: TEXCOORD1);
    emitVertex(p[1] : POSITION, n : TEXCOORD2, uv1: TEXCOORD0,  pw[1]: TEXCOORD1);
    emitVertex(p[3] : POSITION, n : TEXCOORD2, uv3: TEXCOORD0, pw[3]: TEXCOORD1);
    emitVertex(p[2] : POSITION, n2 : TEXCOORD2, uv2: TEXCOORD0, pw[2]: TEXCOORD1);
}


float4 displayFp(
	float4 p: POSITION
	,float2 uv: TEXCOORD0
	,float4 pw: TEXCOORD1
	,float3 normal: TEXCOORD2,
	uniform sampler2D texture) : COLOR
 {
  	float3 lightDir = normalize(float3( 0.5, 1, 0.5 ));
 	float4 lightColor = {1,1,1,1};
 	float kd = 0.5;
 	float ke = 0.3;
 	
 	float4 texColor = tex2D(texture, uv);
		
 	
 	float4 diffuse = kd*max(dot(-normal, lightDir),0)*lightColor;
 	return ke*texColor + diffuse; 
}
