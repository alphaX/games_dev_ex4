/*
 * Texture2D.cpp
 *
 *  Created on: May 16, 2013
 *      Author: alex
 */

#include <stdio.h>
#include <stdlib.h>
#include "Texture2D.h"

Texture2D::Texture2D(const char* filepath) {
	glGenTextures(1,&texture); //allocate the memory for texture
	glBindTexture(GL_TEXTURE_2D,texture); //Binding the texture

	if(!glfwLoadTexture2D(filepath, GLFW_BUILD_MIPMAPS_BIT)){
		fprintf(stderr, "Error opening %s\n", filepath);
		exit(0);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

GLuint Texture2D::getTextureBuffer() {
	return texture;
}

Texture2D::~Texture2D() {
	// TODO Auto-generated destructor stub
}

