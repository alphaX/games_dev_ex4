/*
 * DebugShaderProxy.cpp
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#include "DebugShaderProxy.h"

DebugShaderProxy::DebugShaderProxy(CGcontext cgContext, const char * fxFilepath):ShaderProxy(cgContext, fxFilepath) {
	cgVprofile = cgGLGetLatestProfile(CG_GL_VERTEX);
	cgFprofile = cgGLGetLatestProfile(CG_GL_FRAGMENT);

	vertexShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgVprofile, "debugVS", 0);
	cgGLLoadProgram(vertexShader);
	pixelShader = cgCreateProgramFromFile(cgContext, CG_SOURCE, fxFilepath, cgFprofile, "debugPS", 0);
	cgGLLoadProgram(pixelShader);

	wvp = cgGetNamedParameter(vertexShader, "wvp");
	position = cgGetNamedParameter(vertexShader, "pos");
}

void DebugShaderProxy::setWvp(glm::mat4 wvp){
	cgGLSetMatrixParameterfc(this->wvp, glm::value_ptr(wvp));
}

void DebugShaderProxy::setPositionWithCurrentBuffers(){
	cgGLEnableClientState(position);
	cgGLSetParameterPointer(position, 3, GL_FLOAT, 0, 0);
}

void DebugShaderProxy::preRender(){
	cgGLBindProgram(this->vertexShader);    // note - need vertex program for generic attribs to work!
	cgGLEnableProfile(cgVprofile);

	cgGLBindProgram(this->pixelShader);
	cgGLEnableProfile(cgFprofile);
}

void DebugShaderProxy::postRenderClean(){
	cgGLDisableClientState(position);
	cgGLDisableProfile(cgVprofile);
	cgGLDisableProfile(cgFprofile);
}

DebugShaderProxy::~DebugShaderProxy() {
	// TODO Auto-generated destructor stub
}

