/*
 * ShaderProxy.h
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#ifndef SHADERPROXY_H_
#define SHADERPROXY_H_


#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include <Cg/cgGL.h>

class ShaderProxy {
public:
	ShaderProxy(CGcontext cgContext, const char * fxFilepath);
	virtual ~ShaderProxy();

	CGparameter getParameter(CGprogram & program, const char * name);
protected:
	const char * fxFilepath;
	CGcontext cgContext;
	CGprofile cgVprofile, cgFprofile, cgGprofile;
};

#endif /* SHADERPROXY_H_ */
