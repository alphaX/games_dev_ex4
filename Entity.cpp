/*
 * Entity.cpp
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#include "Entity.h"

Entity::Entity(Mesh & mesh) : mesh(mesh) {
}

void Entity::setWorldMatrix(glm::mat4 worldMatrix){
	world = worldMatrix;
}

glm::mat4 Entity::getWorldMatrix(){
	return world;
}

Mesh & Entity::getMesh(){
	return mesh;
}

void Entity::applyTexture(CubeTexture * texture, std::vector<glm::vec3> textureCoords){
	this->texture = texture;
	this->textureCoords = textureCoords;
}

CubeTexture & Entity::getTexture() {
	return *texture;
}

bool Entity::hasTexture(){
	return textureCoords.size() > 0;
}

std::vector<glm::vec3> & Entity::getTextureCoords(){
	return textureCoords;
}

GLuint Entity::getTextureBuffer(){
	return texture->getTextureBuffer();
}

Entity::~Entity() {
	// TODO Auto-generated destructor stub
}

