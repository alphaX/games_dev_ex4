/*
 * GLRenderer.h
 *
 *  Created on: Apr 27, 2013
 *      Author: alex
 */

#ifndef DEBUGRENDERER_H_
#define DEBUGRENDERER_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <glm/glm.hpp>

#include "DebugShaderProxy.h"
#include "Entity.h"
#include "Camera.h"

class DebugRenderer {
public:
	DebugRenderer(Entity & entity, Camera & camera, DebugShaderProxy * shaderProxy);
	virtual ~DebugRenderer();
	void setProjection(glm::mat4 projection);
	void render();
private:
	glm::mat4 projection;
	Entity & entity;
	Camera & camera;
	DebugShaderProxy * shaderProxy;

	GLuint verticeBuffer;
	GLuint indexBuffer;
	GLuint textureCoordsBuffer;

	void bufferVertices();
};

#endif /* GLRENDERER_H_ */
